
from django.contrib import admin
from django.urls import path
from twitterclone.authentication.urls import urlpatterns as auth_urls
from twitterclone.notification.urls import urlpatterns as notif_urls
from twitterclone.tweet.urls import urlpatterns as tweet_urls
from twitterclone.twitteruser.urls import urlpatterns as user_urls


urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += auth_urls
urlpatterns += notif_urls
urlpatterns += tweet_urls
urlpatterns += user_urls