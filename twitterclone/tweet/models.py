from django.db import models
from django.utils import timezone

class Tweet(models.Model):
    user = models.ForeignKey('twitteruser.TwitterUser', on_delete=models.CASCADE)
    time_submit = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=140)