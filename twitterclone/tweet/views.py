
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, HttpResponseRedirect, reverse
from django.contrib.auth import login
from twitterclone.twitteruser.models import TwitterUser
from twitterclone.notification.models import Notification
from .models import Tweet
from .forms import *
import re


def add_tweet(request):
    if request.method == "POST":
        form = AddTweetForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            mentioned_users = re.findall('@\S+', data["content"])

            tweet = Tweet.objects.create(
                user=request.user,
                content=data["content"],
            )

            for mentioned_user in mentioned_users:
                user = TwitterUser.objects.get(username=mentioned_user[1:])
                notification = Notification.objects.create(
                    user=request.user,
                    tweet=tweet
                )

                user.notifications.add(notification)

            return HttpResponseRedirect(
                request.GET.get('next', reverse('homepage'))
            )

    form = AddTweetForm()
    return render(request, 'loggedin_wrapper.html', {
        'User': request.user.username,
        'content': {
            'content': {"form": form},
            'page': 'generic_form.html'
        }})


def tweet_page(request, id):
    tweet = Tweet.objects.get(pk=id)
    if not request.user.is_anonymous:
        return render(request, 'loggedin_wrapper.html', {
            'User': request.user.username,
            'content': {
                'content': {"tweet": tweet},
                'page': 'tweet.html'
            }})
    else:
        return render(request, 'loggedin_wrapper.html', {
            'User': request.user.username,
            'content': {
                'content': {"tweet": tweet},
                'page': 'tweet.html'
            }})
