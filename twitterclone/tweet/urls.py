from django.contrib import admin
from django.urls import path
from django.contrib.auth.admin import UserAdmin
from .models import Tweet
from .views import *

admin.site.register(Tweet)


urlpatterns = [
  path('tweet/', add_tweet, name='tweet'),
  path('<int:id>/', tweet_page, name='tweett')
]