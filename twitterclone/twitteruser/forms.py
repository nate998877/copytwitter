from django import forms
from .models import TwitterUser

class AddTwitterUser(forms.Form):
    username = forms.CharField(max_length=50)
    email    = forms.CharField(max_length=50, widget=forms.EmailInput)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)