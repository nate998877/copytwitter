from django.contrib import admin
from django.urls import path
from django.contrib.auth.admin import UserAdmin
from .models import TwitterUser
from .views import *

admin.site.register(TwitterUser)


urlpatterns = [
  path('add_user/', add_twitteruser, name='add_user'),
  path('<slug:username>/follow/', follow, name='follow'),
  path('<slug:username>/unfollow/', unfollow, name='unfollow'),
  path("<slug:username>/", userpage, name="profile"),
  path('', homepage, name="homepage"),
]