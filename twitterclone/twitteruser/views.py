
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, HttpResponseRedirect, reverse
from django.contrib.auth import login
from twitterclone.tweet.models import Tweet
from .forms import AddTwitterUser
from .models import TwitterUser


def add_twitteruser(request):
    if request.method == "POST":
        form = AddTwitterUser(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = TwitterUser.objects.create_user(
                email=data["email"],
                username=data["username"],
                password=data["password"],
            )

            login(request, user)
            return HttpResponseRedirect(
                request.GET.get('next', reverse('homepage'))
            )

    form = AddTwitterUser()
    return render(request, 'generic_form_wrapper.html', {'content':{
        'content':{'form': form,},
        'html': "<a href='/login'>back</a>"
    }})


@login_required
def homepage(request):
    tweets = Tweet.objects.filter(user=request.user)
    for user in request.user.following.all():
        u_tweets = Tweet.objects.filter(user=user)
        tweets = tweets | u_tweets
    tweets = tweets.order_by('-time_submit')
    return render(request, 'loggedin_wrapper.html', {
        "User": request.user.username,
        "content": {
            'content': {'tweets':tweets},
            "page": "homepage.html"
        }
    })


def userpage(request, username):
    user = TwitterUser.objects.get(username=username)
    total_tweets = len(Tweet.objects.filter(user=user))
    total_follow = len(user.following.all())
    if not request.user.is_anonymous:
        following = False
        if user in request.user.following.all():
            following = True
        return render(request, 'loggedin_wrapper.html', {
            'User': request.user.username,
            'content': {
                'page':'user_page.html',
                'content': {
                    'user':user, 
                    'logged':True,
                    'is_following':following,
                    'total_tweets':total_tweets,
                    'total_follow':total_follow,
                    }}})
    else:
        return render(request, 'user_page.html', {'content':{
            'user':user, 
            'total_tweets':total_tweets,
            'total_follow':total_follow,
            'logged':False
        }})


@login_required
def follow(request, username):
    follow = TwitterUser.objects.get(username=username)
    request.user.following.add(follow)
    return HttpResponseRedirect("/"+username+"/")



@login_required
def unfollow(request, username):
    follow = TwitterUser.objects.get(username=username)
    request.user.following.remove(follow)
    return HttpResponseRedirect("/"+username+"/")