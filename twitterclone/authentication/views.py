from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect, reverse
from twitterclone.authentication.forms import LoginForm


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if user:= (
                    authenticate(
                        email=data["email"],
                        password=data["password"]
                    )
                ):
                login(request, user)
                return HttpResponseRedirect(
                    request.GET.get('next', reverse('homepage'))
                )


    form = LoginForm()
    return render(request, 'login_wrapper.html', {'content':{'form':form}})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))
