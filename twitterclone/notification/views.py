
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, HttpResponseRedirect, reverse
from django.contrib.auth import login
from .models import Notification


def notifications(request):
    notifications = request.user.notifications.filter(read=False)
    for notification in notifications:
        notification.read = True
        notification.save()
    if not notifications:
        notifications = None
    print(notifications)
    return render(request, 'loggedin_wrapper.html', {
        'User':request.user.username,
        "content":{ 
            'content':{
                    "notifications":notifications
                    },
            'page':'notifications.html'
            }})