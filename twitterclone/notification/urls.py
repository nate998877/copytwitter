from django.contrib import admin
from django.urls import path
from django.contrib.auth.admin import UserAdmin
from .models import Notification
from .views import *

admin.site.register(Notification)

urlpatterns = [
  path('notifications/', notifications, name='notifications'),
]