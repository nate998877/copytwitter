from django.db import models

class Notification(models.Model):
    user = models.ForeignKey("twitteruser.TwitterUser", on_delete=models.DO_NOTHING)
    tweet = models.ForeignKey("tweet.Tweet", on_delete=models.DO_NOTHING)
    read = models.BooleanField(default=False)