from django import template

register = template.Library()

@register.filter
def addstr(val, arg):
    return str(val)+str(arg)